package com.designpatterns;

import android.os.Handler;
import android.util.Log;

import java.util.Observable;

public class MyObservable extends Observable {
    private String name;
    private int age;
    private static  MyObservable INSTACE=null;
    private MyObservable(){
        Log.e("in constructor","getting data");
        getNewDataFromRemote();
    }
    public  static  MyObservable getInstance(){
        if(INSTACE==null){
            INSTACE=new MyObservable();
        }
        return  INSTACE;
    }

    private void getNewDataFromRemote() {
        Log.e("in handler called","getting data");
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e("in set data called","getting data");
                setUserData("Malti Devnani", 24);
                clearChanged();
            }
        }, 7000);
    }

    private void setUserData(String s, int i) {
        name = s;
        age = i;
        //call set change to know that data has been change
        setChanged();
        notifyObservers();
    }
    public String getFullName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
