package com.designpatterns;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.designpatterns.databinding.ActivityMainBinding;

import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity implements Observer {
    private Observable myObservable;
    private ActivityMainBinding activityMainBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("in on create","hii");
        activityMainBinding= DataBindingUtil.setContentView(this,R.layout.activity_main);
        myObservable=MyObservable.getInstance();
        myObservable.addObserver(this);
        activityMainBinding.name.setText("Name will change soon");
        activityMainBinding.age.setText("Age will change soon");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("in on Resume","getting data");
        myObservable=MyObservable.getInstance();
        Log.e("afterResume","getting data");
        myObservable.addObserver(this);
    }

    @Override
    public void update(Observable observable, Object o) {
        if(observable instanceof MyObservable){
            MyObservable myObservable=(MyObservable)observable;
            activityMainBinding.name.setText(myObservable.getFullName());
            activityMainBinding.age.setText(String.valueOf(myObservable.getAge()));
            }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        myObservable.deleteObserver(this);
    }
}
